const util = require("util");
const assert = require("assert");
const Emitter = require("events");
const compose = require("@malijs/compose");
const grpc = require("@grpc/grpc-js");
const protoLoader = require("@grpc/proto-loader");
const logger = require("@malijs/logger");
const { default: wrapServerWithReflection } = require("grpc-node-server-reflection");

const _ = require("./lo");
const Context = require("./context");
const { exec } = require("./run");
const mu = require("./utils");
const Request = require("./request");
const Response = require("./response");

/**
	gRPC service on {@link https://mali.js.org/|MaliJS}.
 * @class
 */
class SolarRPC extends Emitter {

	/**
	 * Creates new instance of SolarRPC.
	 * @constructor
	 * @param {String} config.protoPath path to .proto file
	 * @param {Object} config.implementations Object of implementations
	 * @param {Object=} config.loggerOptions Logger options, see {@link https://github.com/malijs/logger#loggeroptions|@malijs/logger}
	 *
	 * @example Start HTTP Server with ./example.proto with default port and endpoint
	 * const implementations = require("./src/methods");
	 * const grpc = new SolarRPC({path: "./example.proto", implementations, { request: true, response: true, timestamp: logger.isoTime }});
	 */
	constructor(config) {
		super();
		this.grpc = grpc;
		this.servers = [];
		this.ports = [];
		this.data = {};
		this.context = new Context();
		if (config.protoPath) {
			this.addService(config.protoPath);
		} else {
			throw new Error("Path not specified");
		}
		if (!config.loggerOptions?.disabled) {
			this.addLogger(config.loggerOptions);
		}
		if (config.implementations) {
			this.implementations = config.implementations;
			this.use(config.implementations);
		} else {
			throw new Error("Implementations not specified");
		}
	}

	addLogger(options) {
		options = options || { request: true, response: true, timestamp: logger.isoTime };
		this.use(logger(options));
	}

	addService(path) {
		const pd = protoLoader.loadSync(path);
		const proto = grpc.loadPackageDefinition(pd);
		const data = mu.getServiceDefinitions(proto);
		const name = Object.keys(data);

		for (const k in data) {
			const v = data[k];
			if (name.indexOf(k) >= 0 || name.indexOf(v.shortServiceName) >= 0) {
				v.middleware = [];
				v.handlers = {};
				for (const method in v.methods) {
					v.handlers[method] = null;
				}
				this.data[k] = v;
			}
		}
	}

	use(service, name, ...fns) {
		if (typeof service === "function") {
			const isFunction = typeof name === "function";
			for (const serviceName in this.data) {
				const _service = this.data[serviceName];
				if (isFunction) {
					_service.middleware = _service.middleware.concat(service, name, fns);
				} else {
					_service.middleware = _service.middleware.concat(service, fns);
				}
			}
		} else if (typeof service === "object") {
			const testKey = Object.keys(service)[0];
			if (typeof service[testKey] === "function" || Array.isArray(service[testKey])) {
				for (const key in service) {
					const val = service[key];
					const serviceName = this._getMatchingServiceName(key);
					if (serviceName) {
						this.data[serviceName].middleware.push(val);
					} else {
						const { serviceName, methodName } = this._getMatchingCall(key);
						if (serviceName && methodName) {
							if (typeof val === "function") {
								this.use(serviceName, methodName, val);
							} else {
								this.use(serviceName, methodName, ...val);
							}
						} else {
							throw new TypeError(`Unknown method: ${key}`);
						}
					}
				}
			} else if (typeof service[testKey] === "object") {
				for (const serviceName in service) {
					for (const middlewareName in service[serviceName]) {
						const middleware = service[serviceName][middlewareName];
						if (typeof middleware === "function") {
							this.use(serviceName, middlewareName, middleware);
						} else if (Array.isArray(middleware)) {
							this.use(serviceName, middlewareName, ...middleware);
						} else {
							throw new TypeError(`Handler for ${middlewareName} is not a function or array`);
						}
					}
				}
			} else {
				throw new TypeError(`Invalid type for handler for ${testKey}`);
			}
		} else {
			if (typeof name !== "string") {
				fns.unshift(name);
				const serviceName = this._getMatchingServiceName(service);
				if (serviceName) {
					const sd = this.data[serviceName];
					sd.middleware = sd.middleware.concat(fns);
					return;
				} else {
					const { serviceName, methodName } = this._getMatchingCall(service);
					if (!serviceName || !methodName) {
						throw new Error(`Unknown identifier: ${service}`);
					}
					this.use(serviceName, methodName, ...fns);
					return;
				}
			}
			const serviceName = this._getMatchingServiceName(service);
			if (!serviceName) {
				throw new Error(`Unknown service ${service}`);
			}
			const sd = this.data[serviceName];
			let methodName;
			for (const _methodName in sd.methods) {
				if (this._getMatchingHandlerName(sd.methods[_methodName], _methodName, name)) {
					methodName = _methodName;
					break;
				}
			}
			if (!methodName) {
				throw new Error(`Unknown method ${name} for service ${serviceName}`);
			}
			if (sd.handlers[methodName]) {
				throw new Error(`Handler for ${name} already defined for service ${serviceName}`);
			}
			sd.handlers[methodName] = sd.middleware.concat(fns);
		}
	}

	callback(descriptor, mw) {
		const handler = compose(mw);
		if (!this.listeners("error").length) {
			this.on("error", this.onerror);
		}
		return (call, callback) => {
			const context = this._createContext(call, descriptor);
			return exec(context, handler, callback);
		};
	}

	onerror(err) {
		assert(err instanceof Error, `non-error thrown: ${err}`);

		if (this.silent) {
			return;
		}

		const msg = err.stack || err.toString();
		console.error();
		console.error(msg.replace(/^/gm, "  "));
		console.error();
	}

	/**
	 * Start SolarRPC service.
	 * @param {String=} host Host, default: process.env.GRPC_HOST || "localhost"
	 * @param {Number=} port Port, default: process.env.GRPC_PORT || 5000
	 */
	async start(host, port) {
		host = host || process.env.GRPC_HOST || "localhost";
		port = port || process.env.GRPC_PORT || 5000;
		const url = `${host}:${port}`;
		const credentials = this.grpc.ServerCredentials.createInsecure();

		const server = wrapServerWithReflection(new this.grpc.Server());

		server.tryShutdownAsync = util.promisify(server.tryShutdown);
		const bindAsync = util.promisify(server.bindAsync).bind(server);

		for (const sn in this.data) {
			const sd = this.data[sn];
			const handlerValues = Object.values(sd.handlers).filter(Boolean);
			const hasHandlers = handlerValues && handlerValues.length;

			if (sd.handlers && hasHandlers) {
				const composed = {};

				for (const k in sd.handlers) {
					const v = sd.handlers[k];

					if (!v) {
						continue;
					}

					const md = sd.methods[k];
					const shortComposedKey = md.originalName || _.camelCase(md.name);

					composed[shortComposedKey] = this.callback(sd.methods[k], v);
				}

				server.addService(sd.service, composed);
			}
		}

		const bound = await bindAsync(url, credentials);
		if (!bound) {
			throw new Error(`Failed to bind to port: ${url}`);
		}

		this.ports.push(bound);

		server.start();
		this.servers.push({
			server, url
		});

		const serviceName = process.env.SERVICE_NAME || "gRPC Service";
		console.info(`${serviceName} is up and running on ${url}`);
		console.info(`${serviceName} endpoints: ${Object.keys(this.implementations)} `);

		return server;
	}

	_createContext(call, descriptor) {
		const type = mu.getCallTypeFromCall(call) || mu.getCallTypeFromDescriptor(descriptor);
		const { name, fullName, service } = descriptor;
		const pkgName = descriptor.package;
		const context = new Context();
		Object.assign(context, this.context);
		context.request = new Request(call, type);
		context.response = new Response(call, type);
		Object.assign(context, {
			name, fullName, service, app: this, package: pkgName, locals: {} // set fresh locals
		});

		return context;
	}

	_getMatchingServiceName(key) {
		if (this.data[key]) {
			return key;
		}

		for (const serviceName in this.data) {
			if (serviceName.endsWith("." + key)) {
				return serviceName;
			}
		}

		return null;
	}

	_getMatchingCall(key) {
		for (const _serviceName in this.data) {
			const service = this.data[_serviceName];

			for (const _methodName in service.methods) {
				const method = service.methods[_methodName];

				if (this._getMatchingHandlerName(method, _methodName, key)) {
					return { methodName: key, serviceName: _serviceName };
				}
			}
		}

		return { serviceName: null, methodName: null };
	}

	_getMatchingHandlerName(handler, name, value) {
		return name === value || name.endsWith("/" + value) || (handler?.originalName === value) || (handler?.name === value) || (handler && _.camelCase(handler.name) === _.camelCase(value));
	}
}

module.exports = SolarRPC;
